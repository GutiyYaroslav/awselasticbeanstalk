package com.example.springbootforaws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootforAwsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootforAwsApplication.class, args);
    }
//new comment to check changes in repository
}
